import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import java.util.Scanner;

public class CalculatorHelper {

    private static final Logger LOG = (Logger) LogManager.getLogger(CalculatorHelper.class);
    private int num1,num2;
    private double answer;

    public CalculatorHelper() {
        this.num1 = 0;
        this.num2 = 0;

        LOG.info("Calculator Initialized num1 = {} num2 = {}",num1,num2);
    }

    public int getNum1() {
        return num1;
    }

    public void setNum1(int num1) {
        this.num1 = num1;
    }

    public int getNum2() {
        return num2;
    }

    public void setNum2(int num2) {
        this.num2 = num2;
    }

    public void inputNumbers(){
        System.out.println("Enter number1 : ");
        Scanner inputNum1 = new Scanner(System.in);
        num1 = inputNum1.nextInt();
        System.out.println("Enter number2 : ");
        Scanner inputNum2 = new Scanner(System.in);
        num2 = inputNum2.nextInt();
    }

    public int add(){
        LOG.info("Add method Called");
        return num1+num2;
    }

    public int subtract(){
        LOG.info("Subtract method Called");
        return num1-num2;
    }

    public double divide() {
        LOG.info("Divide method Called");

        double result = 0;
        try {
            result = num1 / num2;
        } catch (ArithmeticException e) {
            System.out.println("Exception caught: Division by zero.");
        }
        return result;
    }

    public int multiply(){
        LOG.info("Multiply method Called");
        return num1*num2;
    }
}
