public class Main {
    public static void main(String[] args) {
        System.out.println("Hello");

        CalculatorHelper calculatorHelper = new CalculatorHelper();
        calculatorHelper.inputNumbers();

        int addResult = calculatorHelper.add();
        System.out.println(addResult);

        int subtractResult = calculatorHelper.subtract();
        System.out.println(subtractResult);

        int multiplyResult = calculatorHelper.multiply();
        System.out.println(multiplyResult);

        double divisionResult = calculatorHelper.divide();
        System.out.println(divisionResult);

    }
}
